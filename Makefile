# Debhelper can't change filenames when installing, so make a local directory structure
# Install to / by default or wherever a chroot is pointing
DESTDIR ?=
HAVE_PANDOC := $(shell pandoc -v 2>/dev/null)


ifdef HAVE_PANDOC
scaleimp: scaleimp.tcl README.html
else
scaleimp: scaleimp.tcl
endif
	cp scaleimp.tcl scaleimp

README.html:
	pandoc -t html5 README.md -o README.html

.PHONY: install
install:
	mkdir -p ${DESTDIR}/usr/bin
	mkdir -p ${DESTDIR}/usr/share/icons/hicolor/16x16/apps/
	mkdir -p ${DESTDIR}/usr/share/icons/hicolor/24x24/apps/
	mkdir -p ${DESTDIR}/usr/share/icons/hicolor/32x32/apps/
	mkdir -p ${DESTDIR}/usr/share/icons/hicolor/48x48/apps/
	mkdir -p ${DESTDIR}/usr/share/icons/hicolor/128x128/apps/
	mkdir -p ${DESTDIR}/usr/share/applications/
	mkdir -p ${DESTDIR}/usr/share/doc/scaleimp/
	install scaleimp.tcl ${DESTDIR}/usr/bin/scaleimp
	install scaleimp16.png ${DESTDIR}/usr/share/icons/hicolor/16x16/apps/scaleimp.png -m 644
	install scaleimp24.png ${DESTDIR}/usr/share/icons/hicolor/24x24/apps/scaleimp.png -m 644
	install scaleimp32.png ${DESTDIR}/usr/share/icons/hicolor/32x32/apps/scaleimp.png -m 644
	install scaleimp48.png ${DESTDIR}/usr/share/icons/hicolor/48x48/apps/scaleimp.png -m 644
	install scaleimp128.png ${DESTDIR}/usr/share/icons/hicolor/128x128/apps/scaleimp.png -m 644
	install scaleimp.desktop ${DESTDIR}/usr/share/applications/scaleimp.desktop -m 644
ifdef HAVE_PANDOC
	install README.html ${DESTDIR}/usr/share/doc/scaleimp/README.html -m 644
	install screenshots.png ${DESTDIR}/usr/share/doc/scaleimp/screenshots.png -m 644
endif
	install README.md ${DESTDIR}/usr/share/doc/scaleimp/README.md -m 644

.PHONY: uninstall
uninstall:
	rm -rf ${DESTDIR}/usr/bin/scaleimp
	rm -rf ${DESTDIR}/usr/share/icons/hicolor/16x16/apps/scaleimp.png
	rm -rf ${DESTDIR}/usr/share/icons/hicolor/24x24/apps/scaleimp.png
	rm -rf ${DESTDIR}/usr/share/icons/hicolor/32x32/apps/scaleimp.png
	rm -rf ${DESTDIR}/usr/share/icons/hicolor/48x48/apps/scaleimp.png
	rm -rf ${DESTDIR}/usr/share/icons/hicolor/128x128/apps/scaleimp.png
	rm -rf ${DESTDIR}/usr/share/applications/scaleimp.desktop
	rm -rf ${DESTDIR}/usr/share/doc/scaleimp

.PHONY: clean
clean:
	rm -f scaleimp README.html
	rm -fr usr

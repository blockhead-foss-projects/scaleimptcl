1.0.2
* README now ships with the app. No program changes.

1.0.1
* Fix numpad input for Linux and macOS.
* Bind keyboard navigation modifier to Cmd on Mac.
* macOS is now supported.
* The program can be entirely built on the command line without any GUI elements
at all now.
* Bind Ctrl-A to select all text in text inputs under Linux.
* Wrote the end user manual up
* Improved and streamlined the self-compilation processes.
* ScaleImp is now built and released through GitLab CI/CD.

1.0.0
* Initial Release

ScaleImp has now been re-released as version "1.0.2". This version contains no
actual program changes but the README will now ship with the program for your
easy reference.

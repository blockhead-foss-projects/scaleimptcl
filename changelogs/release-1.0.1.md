ScaleImpTcl's first release delivered by Continuous Deployment is here! The new 1.0.1 has received the following improvements over 1.0.0:
* Fix numpad input for Linux and macOS.
* Bind keyboard navigation modifier to Cmd on Mac.
* macOS is now supported.
* The program can be entirely built on the command line without any GUI elements
at all now.
* Bind Ctrl-A to select all text in text inputs under Linux.
* Wrote the end user manual up
* Improved and streamlined the self-compilation processes.
* ScaleImp is now built and released through GitLab CI/CD.
